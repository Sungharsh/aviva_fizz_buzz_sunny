import React, { Component } from "react";
import TextField from "@material-ui/core/TextField";

class InputNumber extends Component {
  render() {
    return (
      <div className="left">
        <center>
          <h2>Enter a number</h2>
          <TextField
            id="standard-number"
            label="Number"
            type="number"
            min="1"
            max="1000"
            required
            pattern="[A-Za-z]{3}"
            multiline
            rowsMax="10"
            margin="normal"
            placeholder="Enter number"
            value={this.props.stateValue}
            onChange={this.props.inputTyped}
          />
          <input
            type="submit"
            value="Enter"
            onClick={this.props.fizzBuzzHandle}
            disabled={this.props.enableSubmit}
          />
        </center>
      </div>
    );
  }
}

export default InputNumber;
