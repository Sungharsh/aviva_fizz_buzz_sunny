export const getClasses = (output) => {
  if (output.toString().includes("Fizz Buzz")) {
    return "none";
  } else if (output.toString().includes("Fizz")) {
    return "fizz";
  } else if (output.toString().includes("Buzz")) {
    return "buzz";
  } else {
    return "none";
  }
};
