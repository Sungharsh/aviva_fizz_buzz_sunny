import React, { Component } from "react";
import Paper from "@material-ui/core/Paper";
import InputNumber from "./components/inputNumber";
import "./App.css";
import { getClasses } from "./components/utils";

class Main extends Component {
  state = {
    value: Number,
    userInput: Number,
    isDisabled: true,
    numberRange: [],
    encriptedValue: [],
  };

  handleSubmit = (event) => {
    event.preventDefault();
    this.setState({ isDisabled: true });
    event.target.reset();
  };
  handleChange = (event) => {
    const userInput = event.target.value;
    if (parseInt(userInput)) {
      this.setState({ isDisabled: false });
    }
    this.setState({ userInput });
  };

  fizzBuzzHandler = () => {
    let userInput = this.state.userInput;
    let assignFizzBuzz = [];
    for (let i = 1; i <= userInput; i++) {
      if (i % 3 === 0 && i % 5 === 0) {
        assignFizzBuzz.push(i + " Fizz Buzz");
      } else if (i % 3 === 0 && i % 5 !== 0) {
        assignFizzBuzz.push(i + " Fizz");
      } else if (i % 5 === 0) {
        assignFizzBuzz.push(i + " Buzz");
      } else {
        assignFizzBuzz.push(i);
      }
    }
    this.setState({
      assignFizzBuzz,
    });
    this.setState({ userInput });
    this.setState({ numberRange: assignFizzBuzz });
  };

  renderOutput = () => {
    const output = this.state.numberRange;
    if (output.length === 0)
      return (
        <strong>
          <p className="child-container" style={{ color: "red" }}>
            Please input a positive number.
          </p>
        </strong>
      );
    return (
      <div>
        <ul className="child-container">
          {output.map((item) => (
            <li className={getClasses(item)} key={item}>
              {item}
            </li>
          ))}
        </ul>
      </div>
    );
  };
  render() {
    return (
      <div className="App">
        <center className="header">
          <h1>Fizz Buzz</h1>
          <p>
            Enter a positive integer in the input field and submit to find Fizz
            Buzz
          </p>
        </center>
        <Paper elevation={10}>
          <form
            className="container"
            onSubmit={this.handleSubmit}
            noValidate
            autoComplete="off"
          >
            <InputNumber
              className="section"
              StateValue={this.state.value}
              inputTyped={this.handleChange}
              enableSubmit={this.state.isDisabled}
              fizzBuzzHandle={this.fizzBuzzHandler}
            />
          </form>
          {this.renderOutput()}
        </Paper>
      </div>
    );
  }
}

export default Main;
